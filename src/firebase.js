import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getStorage } from "firebase/storage";
import { getFirestore } from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyCB2rOOjbqU4iAcjofK24mBeCsMFCRhdIw",
  authDomain: "chatweb-1e123.firebaseapp.com",
  projectId: "chatweb-1e123",
  storageBucket: "chatweb-1e123.appspot.com",
  messagingSenderId: "968473202166",
  appId: "1:968473202166:web:d4b02c643e11a27eef0bd7"
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
export const auth = getAuth();
export const storage = getStorage();
export const db = getFirestore()
